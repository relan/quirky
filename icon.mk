#
# Quirky, QR scanner for Android
# Copyright (C) 2017  Andrew Nayenko
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

source = icon.svg
mipmap = src/main/res/mipmap
densities = mdpi hdpi xhdpi xxhdpi xxxhdpi

all : $(foreach x, $(densities), $(mipmap)-$(x)/ic_launcher.png)

$(mipmap)-mdpi/ic_launcher.png : $(source)
	inkscape $< -e $@ -w 48

$(mipmap)-hdpi/ic_launcher.png : $(source)
	inkscape $< -e $@ -w 72

$(mipmap)-xhdpi/ic_launcher.png : $(source)
	inkscape $< -e $@ -w 96

$(mipmap)-xxhdpi/ic_launcher.png : $(source)
	inkscape $< -e $@ -w 144

$(mipmap)-xxxhdpi/ic_launcher.png : $(source)
	inkscape $< -e $@ -w 192

.PHONY : all
