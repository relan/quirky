/*
 * Quirky, QR scanner for Android
 * Copyright (C) 2017  Andrew Nayenko
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.airpost.relan.quirky;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.BarcodeView;
import java.util.List;


public class MainActivity extends AppCompatActivity {
    private static final String TAG = "Quirky";
    private static int PERMISSION_REQUEST_CODE = 8948;
    private BarcodeView mBarcodeView;
    private String mLastText = "";

    private BarcodeCallback callback = new BarcodeCallback() {
        @Override
        public void barcodeResult(BarcodeResult result) {
            if (result.getText().equals(mLastText)) {
                // Prevent duplicate scans
                return;
            }

            mLastText = result.getText();
            Toast.makeText(getApplicationContext(), mLastText, Toast.LENGTH_LONG).show();
        }

        @Override
        public void possibleResultPoints(List<ResultPoint> resultPoints) {
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);
        mBarcodeView = (BarcodeView) findViewById(R.id.barcode_scanner);

        if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.CAMERA) ==
                PackageManager.PERMISSION_GRANTED) {
            mBarcodeView.decodeContinuous(callback);
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[] { Manifest.permission.CAMERA },
                    PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
            String[] permissions, int[] results) {
        if (requestCode == PERMISSION_REQUEST_CODE
                && results.length > 0
                && results[0] == PackageManager.PERMISSION_GRANTED) {
            recreate();
        } else {
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        mBarcodeView.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();

        mBarcodeView.pause();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return mBarcodeView.onKeyDown(keyCode, event)
            || super.onKeyDown(keyCode, event);
    }

}
